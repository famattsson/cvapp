//
//  CVTableViewCell.swift
//  CVapp
//
//  Created by Filip Mattsson on 2018-11-01.
//  Copyright © 2018 Filip Mattsson. All rights reserved.
//

import UIKit

class CVTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var cvImageView: UIImageView!
    
    @IBOutlet weak var durationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
