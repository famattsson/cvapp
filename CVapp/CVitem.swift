//
//  File.swift
//  CVapp
//
//  Created by Filip Mattsson on 2018-11-01.
//  Copyright © 2018 Filip Mattsson. All rights reserved.
//

import Foundation

class CVitem {
    var title : String
    var imageName: String
    var duration: String
    
    init(title : String, imageName: String, duration : String) {
        self.title = title
        self.imageName = imageName
        self.duration = duration
    }
}
