//
//  DetailViewController.swift
//  CVapp
//
//  Created by Filip Mattsson on 2018-11-02.
//  Copyright © 2018 Filip Mattsson. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    var cvItem : CVitem = CVitem(title: "", imageName: "", duration: "")
    
    @IBOutlet weak var DetailLabel: UINavigationItem!
    @IBOutlet weak var DetailImageView: UIImageView!
    @IBOutlet weak var CvTextView: UITextView!
    @IBOutlet weak var DetailDurationLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        DetailLabel.title = cvItem.title
        DetailImageView?.image = UIImage(named: cvItem.imageName)
        DetailDurationLabel?.text = cvItem.duration
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CvTextView.alpha = 0.0
        DetailImageView.alpha = 0.0
        DetailDurationLabel.alpha = 0.0
        UIView.animate(withDuration: 0.7, animations: {
            self.DetailImageView.center.y += 400
            self.CvTextView.center.x += 400
            self.DetailDurationLabel.center.x -= 400
            self.CvTextView.alpha = 1.0
            self.DetailDurationLabel.alpha = 1.0
            self.DetailImageView.alpha = 1.0
        })
    }

}
