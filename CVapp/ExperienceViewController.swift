//
//  ExperienceViewController.swift
//  CVapp
//
//  Created by Filip Mattsson on 2018-11-01.
//  Copyright © 2018 Filip Mattsson. All rights reserved.
//

import UIKit

class ExperienceViewController: UIViewController {
    var workExperiences = [CVitem(title: "Husqvarna", imageName: "Husqvarna", duration: "2018 - 2018"), CVitem(title: "Cybercom", imageName: "Cybercom", duration: "2018-2018")]
    var educationExperiences = [CVitem(title: "JTH; DMP", imageName: "JTH", duration: "2016 - 2018")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DetailViewController {
            if let indexPath = sender as? IndexPath {
                var experience : CVitem
                if (indexPath.section == 0) {
                    experience = workExperiences[indexPath.row]
                }
                else {
                    experience = educationExperiences[indexPath.row]
                }
                destination.cvItem = experience
            }
        }
    }
}

extension ExperienceViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return workExperiences.count
        }
        else {
            return educationExperiences.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CVTableViewCell", for: indexPath) as? CVTableViewCell {
            if indexPath.section == 0 {
                cell.titleLabel.text = workExperiences[indexPath.row].title
                cell.cvImageView?.image = UIImage(named: workExperiences[indexPath.row].imageName)
                cell.durationLabel.text = workExperiences[indexPath.row].duration
            }
            else if indexPath.section == 1 {
                cell.titleLabel.text = educationExperiences[indexPath.row].title
                cell.cvImageView?.image = UIImage(named: educationExperiences[indexPath.row].imageName)
                cell.durationLabel.text = educationExperiences[indexPath.row].duration
            }
            return cell;
        }
        else {
            return UITableViewCell()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2;
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (section == 0) {
            return "Work"
        }
        else if (section == 1) {
            return "Education"
        }
        else {
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "ExperienceDetailSegue", sender: indexPath)
    }
}
