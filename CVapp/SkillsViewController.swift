//
//  ExperienceViewController.swift
//  CVapp
//
//  Created by Filip Mattsson on 2018-11-01.
//  Copyright © 2018 Filip Mattsson. All rights reserved.
//

import UIKit

class SkillsViewController: UIViewController {
    var languageSkills = [CVitem(title: "C#", imageName: "C#", duration: "2015 - 2018"), CVitem(title: "C++", imageName: "C++", duration: "2016-2018")]
    var toolSkills = [CVitem(title: "Visual Studio", imageName: "VisualStudio", duration: "2016 - 2018"), CVitem(title: "Unity", imageName: "Unity", duration: "2013 - 2018")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DetailViewController {
            if let indexPath = sender as? IndexPath {
                var skill : CVitem
                if (indexPath.section == 0) {
                    skill = languageSkills[indexPath.row]
                }
                else {
                    skill = toolSkills[indexPath.row]
                }
                destination.cvItem = skill
            }
        }
    }
}

extension SkillsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return languageSkills.count
        }
        else {
            return toolSkills.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CVTableViewCell", for: indexPath) as? CVTableViewCell {
            if indexPath.section == 0 {
                cell.titleLabel.text = languageSkills[indexPath.row].title
                cell.cvImageView?.image = UIImage(named: languageSkills[indexPath.row].imageName)
                cell.durationLabel.text = languageSkills[indexPath.row].duration
            }
            else if indexPath.section == 1 {
                cell.titleLabel.text = toolSkills[indexPath.row].title
                cell.cvImageView?.image = UIImage(named: toolSkills[indexPath.row].imageName)
                cell.durationLabel.text = toolSkills[indexPath.row].duration
            }
            return cell;
        }
        else {
            return UITableViewCell()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2;
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (section == 0) {
            return "Languages"
        }
        else if (section == 1) {
            return "Tools"
        }
        else {
            return ""
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "SkillsDetailSegue", sender: indexPath)
    }

}
